FROM debian:jessie-slim
MAINTAINER https://gitlab.com/alelec/docker-ffmpeg-win32-builder

# Install toolchain and associated requirements
RUN apt update && apt install -y build-essential git ca-certificates ed \
    pkg-config gcc-mingw-w64 autoconf autotools-dev automake autogen libtool \
    m4 realpath gettext mercurial yasm nasm wget curl cmake subversion texinfo \
    g++ bison flex cvs gcc make  zlib1g-dev unzip pax libtool-bin
    
# Install ffmpeg-windows-build-helpers
RUN cd / && \
    git clone https://github.com/rdp/ffmpeg-windows-build-helpers.git && \
    chmod u+x /ffmpeg-windows-build-helpers/cross_compile_ffmpeg.sh && \
    mkdir /build

WORKDIR /build

# Do initial run to create the compilers and libraries
RUN /ffmpeg-windows-build-helpers/cross_compile_ffmpeg.sh --build-ffmpeg-static=n --build-ffmpeg-shared=n --compiler-flavors=multi --sandbox-ok=y --prefer-stable=y
        
# Clean up cache
RUN rm -rf /var/cache/apk/*